class Animal
	constructor: (@name) ->
	
	move: (meters) ->
		alert @name + "moved #{meters}m."

class Snake extends Animal
	move: ->
		alert "Slithering..."
		super 5

class Horse extends Animal
	move: ->
		alert "Galloping"
		super 5


sam = new Snake "Sammy the python"
tom = new Horse "Tommy "

sam.move()
tom.move()
