require './prelude'

intensify = (n) -> 
  2


qc.testPure intensify, [qc.arbint],
  (c, arg, result) -> 
    c.guard arg > 0
    arg + 2 == result


qc.testPure intensify, [qc.arbInt],

  (c, arg, result) -> 
    c.guard arg > 0
    arg - 2 == result

qc.testPure intensify, [qc.arbConst(0)],

  (c, arg, result) ->
    result is 0


qc.test()
